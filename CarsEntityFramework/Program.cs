﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Data.Entity;

namespace CarsEntityFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            // Not for prod
            Database.SetInitializer(new DropCreateDatabaseIfModelChange<CarDb>());

            InsertData();
            QueryData();
        }

        private static void QueryData()
        {
            var db = new CarDb();
            db.Database.Log = Console.WriteLine;

            var query = from car in db.Cars
                        orderby car.Combined descending, car.Name ascending
                        select car;

            // var hardQuery = db.Cars.GroupBy(c => c.Manufacturer)
            //                         .Select(g => new 
            //                         {
            //                             Name = g.Key,
            //                             Cars = g.OrderByDescending(c => c.Combined).Take(2)
            //                         });

            foreach (var car in query)
            {
                Console.WriteLine($"{car.Name}: {car.Combined}");
            }
        }

        private static void InsertData()
        {
            var cars = ProcessCarsFile("cars.csv");
            var db = new CarDb();

            if(!db.Cars.Any())
            {
                foreach (var car in cars)
                {
                    db.Cars.Add(car);
                }
                db.SaveChanges();
            }
        }

        private static IEnumerable<string> QueryXML()
        {
            var ns = (XNamespace)"http://pluralsight.com/cars/2016";
            var ex = (XNamespace)"http://pluralsight.com/cars/2016/ex";
            var document = XDocument.Load($"{OUTPUT_PATH}cars.xml");
            var query =
                from element in document.Element(ns + "Cars")?.Elements(ex + "Car") ?? Enumerable.Empty<XElement>()
                where element.Attribute("Manufacturer")?.Value == "BMW"
                select element.Attribute("Name").Value;

            return query;
        }

        private static void CreateXML()
        {
            var records = ProcessCarsFile("cars.csv");
            var ns = (XNamespace)"http://pluralsight.com/cars/2016";
            var ex = (XNamespace)"http://pluralsight.com/cars/2016/ex";
            var document = new XDocument();
            var cars = new XElement(ns + "Cars",

                from record in records
                select new XElement(ex + "Car",
                    new XAttribute("Name", record.Name),
                    new XAttribute("Combined", record.Combined),
                    new XAttribute("Manufacturer", record.Manufacturer))
            );

            cars.Add(new XAttribute(XNamespace.Xmlns + "ex", ex));

            document.Add(cars);
            document.Save($"{OUTPUT_PATH}cars.xml");
        }

        private static List<Car> ProcessCarsFile(string fileName)
        {
            var CarsList = new List<Car> { };
            if (CheckFileExists(fileName))
            {
                return File.ReadAllLines($"{FILE_PATH}{fileName}")
                    .Skip(1)
                    .Where(l => l.Length > 1)
                    .ToCar()
                    .ToList();
            };
            return CarsList;
        }

        private static List<Manufacturer> ProcessManufacturerFile(string fileName)
        {
            var ManufacturerList = new List<Manufacturer> { };
            if (CheckFileExists(fileName))
            {
                return File.ReadAllLines($"{FILE_PATH}{fileName}")
                            .Where(l => l.Length > 1)
                            .Select(l =>
                            {
                                var cols = l.Split(',');
                                return new Manufacturer
                                {
                                    Name = cols[0],
                                    Headquarter = cols[1],
                                    Year = int.Parse(cols[2]),
                                };
                            })
                            .ToList();
            }
            return ManufacturerList;
        }

        private static bool CheckFileExists(string fileName)
        {
            if (File.Exists($"{FILE_PATH}{fileName}"))
            {
                return true;
            }
            else
            {
                var ex = new FileLoadException();
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        private static string FILE_PATH = "./assets/";
        private static string OUTPUT_PATH = "./output/";
    }

}