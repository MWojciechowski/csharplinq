using System.Data.Entity;

namespace CarsEntityFramework
{
    public class CarDb : DbContext
    {
        public DbSet<Car> { get; set; }
    }
}