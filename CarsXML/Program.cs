﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace CarsXML
{
    class Program
    {
        static void Main(string[] args)
        {
            // Should return List of Car
            CreateXML();
            var bmwCars = QueryXML();
            foreach (var car in bmwCars)
            {
                Console.WriteLine(car);
            }

        }

        private static IEnumerable<string> QueryXML()
        {
            var ns = (XNamespace)"http://pluralsight.com/cars/2016";
            var ex = (XNamespace)"http://pluralsight.com/cars/2016/ex";
            var document = XDocument.Load($"{OUTPUT_PATH}cars.xml");
            var query =
                from element in document.Element(ns + "Cars")?.Elements(ex + "Car") ?? Enumerable.Empty<XElement>()
                where element.Attribute("Manufacturer")?.Value == "BMW"
                select element.Attribute("Name").Value;

            return query;
        }

        private static void CreateXML()
        {
            var records = ProcessCarsFile("cars.csv");
            var ns = (XNamespace)"http://pluralsight.com/cars/2016";
            var ex = (XNamespace)"http://pluralsight.com/cars/2016/ex";
            var document = new XDocument();
            var cars = new XElement(ns + "Cars",

                from record in records
                select new XElement(ex + "Car",
                    new XAttribute("Name", record.Name),
                    new XAttribute("Combined", record.Combined),
                    new XAttribute("Manufacturer", record.Manufacturer))
            );

            cars.Add(new XAttribute(XNamespace.Xmlns + "ex", ex));

            document.Add(cars);
            document.Save($"{OUTPUT_PATH}cars.xml");
        }

        private static List<Car> ProcessCarsFile(string fileName)
        {
            var CarsList = new List<Car> { };
            if (CheckFileExists(fileName))
            {
                return File.ReadAllLines($"{FILE_PATH}{fileName}")
                    .Skip(1)
                    .Where(l => l.Length > 1)
                    .ToCar()
                    .ToList();
            };
            return CarsList;
        }

        private static List<Manufacturer> ProcessManufacturerFile(string fileName)
        {
            var ManufacturerList = new List<Manufacturer> { };
            if (CheckFileExists(fileName))
            {
                return File.ReadAllLines($"{FILE_PATH}{fileName}")
                            .Where(l => l.Length > 1)
                            .Select(l =>
                            {
                                var cols = l.Split(',');
                                return new Manufacturer
                                {
                                    Name = cols[0],
                                    Headquarter = cols[1],
                                    Year = int.Parse(cols[2]),
                                };
                            })
                            .ToList();
            }
            return ManufacturerList;
        }

        private static bool CheckFileExists(string fileName)
        {
            if (File.Exists($"{FILE_PATH}{fileName}"))
            {
                return true;
            }
            else
            {
                var ex = new FileLoadException();
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        private static string FILE_PATH = "./assets/";
        private static string OUTPUT_PATH = "./output/";
    }

}