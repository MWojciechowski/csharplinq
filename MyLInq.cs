using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqexploration
{
    public static class MyLinq
    {
        public static int MyCount<T>(this IEnumerable<T> sequence)
        {
            var count = 0;
            foreach (var item in sequence)
            {
                count += 1;
            }
            return count;
        }

        public static IEnumerable<T> MyFilter<T>(this IEnumerable<T> source,
                                                Func<T, bool> predicate)
        {
            foreach (var item in source)
            {
                if (predicate(item))
                {
                    // Deferred Execution - tells IEnumerate child to be as lazy as it can go. That yield keyword will return an item to abstract 
                    //                      IEunumerable container and after cycling through whole data in source it will automatically return 
                    //                      that IEnumerable collection It doesn't work until we try to produce a result!
                    //                      So the IEnumerable won't work until its being tested/checked/achieved. To remove deferred execution 
                    //                      just use .ToDictionary/.ToList, etc methods at the end. If we use MyFilter and MyCount it will run 
                    //                      twice, so you must stop deferred execution to make it only once.
                    //                      To understand which LINQ operator is which go and visit:
                    //                      https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/linq/classification-of-standard-query-operators-by-manner-of-execution
                    yield return item;
                }
            }
        }

        public static IEnumerable<double> Random()
        {
            var random = new Random();
            while(true)
            {
                yield return random.NextDouble();
            }
        }
    }
}