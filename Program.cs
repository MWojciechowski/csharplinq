﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

// Words of wisdom:
// 1. Working with streaming operators in LINQ allows us to work with datasrouce that is almost (theoretically) infinitely long.
// 2. To find which operator is which check: https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/linq/classification-of-standard-query-operators-by-manner-of-execution

namespace linqexploration
{
    class Program
    {
        static void Main(string[] args)
        {
            var devs = new Employee[]
            {
                new Employee { Id = 1, Name = "Kevin" },
                new Employee { Id = 2, Name = "Marc" },
                new Employee { Id = 2, Name = "Paul" },
                new Employee { Id = 2, Name = "Steve" }
            };

            // Both are equal:
            var syntaxMethodQuery = devs.Where(e => e.Name.Length == 5).OrderByDescending(e => e.Name);
            var syntaxQueryQuery = from dev in devs
                                   where dev.Name.Length == 5
                                   orderby dev.Name descending
                                   select dev;

            // foreach (var employee in syntaxQueryQuery)
            // {
            //     Console.WriteLine(employee.Name);
            // }

            var movies = new List<Movie>
            {
                new Movie { Title="The Dark Knight", Rating = 9.0f, Year = 2008 },
                new Movie { Title="Godfather", Rating = 9.1f, Year = 1972 },
                new Movie { Title="Inception", Rating = 8.7f, Year = 2010 },
                new Movie { Title="12 Angry Men", Rating = 8.9f, Year = 1957 },
                new Movie { Title="Schindler's List", Rating = 8.9f, Year = 1993 },
                new Movie { Title="Pulp Fiction", Rating = 8.8f, Year = 1994 },
                new Movie { Title="Fight Club", Rating = 8.8f, Year = 1999 }
            };

            var moviesAfter2000 = movies.Where(m => m.Year >= 2000).OrderBy(m => m.Title);
            var myLinqMoviesAfter200 = movies.MyFilter(m => m.Year >= 2000);
            var originalLinqFilterMoviesAfter2000 = movies.Where(m => m.Year >= 2000);
            // Blocking deferred exception (definition go to linqexploration.MyLing.MyFilter):
            var defBlockedLinqMoviesAfter2000 = movies.Where(m => m.Year >= 2000).ToList();

            // foreach (var movie in defBlockedLinqMoviesAfter2000)
            // {
            //     Console.WriteLine(movie.Title);
            // }

            // Proof for streaming operators 

            // var numbers = MyLinq.Random().Where(n => n > 0.5).Take(100000);
            // foreach (var number in numbers)
            // {
            //     Console.WriteLine(number);
            // }
            // Console.Write(numbers.MyCount());
        }

        private static void ShowLargeFilesWithLinq(string path)
        {
            IEnumerable<FileInfo> query = new DirectoryInfo(path).GetFiles().OrderByDescending(f => f.Length).Take(5);

            foreach (FileInfo file in query)
            {
                Console.WriteLine($"{file.Name,-20} : {file.Length,10:N0} bytes");
            }
        }

        private static void ShowLargeFilesWithoutLinq(string path)
        {
            DirectoryInfo directory = new DirectoryInfo(path);
            FileInfo[] files = directory.GetFiles();
            Array.Sort(files, new FileSizeComparer());
            Head(files);
        }



        public class FileSizeComparer : IComparer<FileInfo>
        {
            public int Compare([AllowNull] FileInfo x, [AllowNull] FileInfo y)
            {
                return y.Length.CompareTo(x.Length);
            }
        }

        private static void Head(FileInfo[] files, int? times = 5)
        {
            for (int i = 0; i < times; i++)
            {
                FileInfo file = files[i];
                Console.WriteLine($"{file.Name,-20} : {file.Length,10:N0} bytes");
            }
        }
    }
}

// Commented out section

// Main() {} area below:

// string path = @"C:\windows";
// ShowLargeFilesWithoutLinq(path);
// Console.WriteLine("**************************");
// ShowLargeFilesWithLinq(path);
// ----------------------------------------------------------------------------------
// Actual project:

// foreach (Employee person in devs)
// {
//     Console.WriteLine(person.Name);
// }
// Code above can be done the hard way. Using IEnumerable (See comments P1C0);

// P1C0
// Just paste it to Main to see it working. Remember to comment out the "using System.Linq;"

// IEnumerable<Employee> gameDevs = new Employee[]
//             {
//                 new Employee { Id = 0, Name = "Gamer Martin" }
//             };
// IEnumerator<Employee> enumerator = gameDevs.GetEnumerator();
//             while (enumerator.MoveNext())
//             {
//                 Console.WriteLine(enumerator.Current.Name);
//             }