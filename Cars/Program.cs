﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Cars
{
    class Program
    {
        static void Main(string[] args)
        {
            // Should return List of Car
            var cars = ProcessCarsFile("cars.csv");
            var manu = ProcessManufacturerFile("manufacturers.csv");


            //todo Car queries
            var carsMostEfficientQuery = cars.OrderByDescending(c => c.Combined).ThenBy(c => c.Name);
            var carsMostEfficientBMW2016Query = cars
                .Where(c => c.Manufacturer == "BMW" && c.Year == 2016)
                .OrderByDescending(c => c.Combined)
                .ThenBy(c => c.Name);
            var carsIsAnyCarNotFrom2016 = cars.Any(c => c.Year != 2016);
            // Console.WriteLine(isThereAnyCarNotFrom2016);

            // Now we can work with a copy of that specified data, sometimes its much faster
            var carsOnlySpecificDataQuery =
                from car in cars
                where car.Manufacturer == "Ford" && car.Year == 2016
                orderby car.Combined descending, car.Name ascending
                select new
                {
                    car.Manufacturer,
                    car.Name,
                    car.Cylinders,
                    car.Combined
                };

            // foreach (var car in onlySpecificDataQuery.Take(10))
            // {
            //     Console.WriteLine(
            //         $"{car.Manufacturer,-32} | {car.Name,-48} | {car.Cylinders,-8} | {car.Combined,-8}");
            // }


            // Manufacturer queries
            var manuAllDataQuery = manu.OrderBy(m => m.Name);
            // foreach (var manufacturer in manuAllDataQuery)
            // {
            //     Console.WriteLine($"{manufacturer.Name, -48}|{manufacturer.Headquarter, -16}|{manufacturer.Year, -4}");
            // }


            //todo Combined queries
            var combineCarsManuSyntaxQuery =
                    from car in cars
                    join manufacturer in manu
                        on car.Manufacturer equals manufacturer.Name
                    orderby car.Combined descending, car.Name ascending
                    select new
                    {
                        manufacturer.Headquarter,
                        car.Name,
                        car.Combined
                    };

            var combineCarsManuMethodQuery = cars.Join(
                                                    manu, c => c.Manufacturer,
                                                    m => m.Name,
                                                    (c, m) => new
                                                    {
                                                        m.Headquarter,
                                                        c.Name,
                                                        c.Combined
                                                    }).OrderByDescending(c => c.Combined)
                                                    .ThenBy(c => c.Name);

            // Join against two data (or more...) You have to make data inside object named the same. So we change Manufacturer.Name to Manufacturer.Manufacturer:
            var combineTwoDataCarsManuMethodQuery = cars.Join(
                                                            manu, c => new { c.Manufacturer, c.Year },
                                                            m => new { Manufacturer = m.Name, m.Year },
                                                            (c, m) => new
                                                            {
                                                                m.Headquarter,
                                                                c.Name,
                                                                c.Combined
                                                            }).OrderByDescending(c => c.Combined)
                                                            .ThenBy(c => c.Name);

            // string country = "Country";
            // string model = "Model";
            // string combined = "Combined [miles per galon]";
            // Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------");
            // Console.WriteLine($"{country,-48}|{model,-48}|{combined,-26}");
            // Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------");
            // foreach (var car in combineTwoDataCarsManuMethodQuery.Take(10))
            // {
            //     Console.WriteLine($"{car.Headquarter,-48}|{car.Name,-48}|{car.Combined,-12}");
            // }


            //todo Groupinng
            var carsGroupByManufacturer = cars.GroupBy(c => c.Manufacturer);

            // foreach (var carGroup in carsGroupByManufacturer)
            // {
            //     Console.WriteLine($"{carGroup.Key, -32} has {carGroup.Count(), -3} cars.");
            // }
            // foreach (var carGroup in carsGroupByManufacturer)
            // {
            //     Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            //     Console.WriteLine(carGroup.Key);
            //     foreach (var car in carGroup.OrderByDescending(c => c.Combined).Take(3))
            //     {
            //         Console.WriteLine($"-> {car.Name, -36} | {car.Combined}");
            //     }
            // }

            var carsGroupJoinManufacturer =
                manu.GroupJoin(cars, m => m.Name, c => c.Manufacturer,
                (m, g) =>
                    new
                    {
                        Manufacturer = m,
                        Cars = g
                    })
                .OrderBy(m => m.Manufacturer.Name);
            // foreach (var group in carsGroupJoinManufacturer)
            // {
            //     Console.WriteLine($"{group.Manufacturer.Name}:{group.Manufacturer.Headquarter}");
            //     foreach (var car in group.Cars.OrderByDescending(c => c.Combined).Take(3))
            //     {
            //         Console.WriteLine($"\t\t{car.Name}:{car.Combined}");
            //     }
            // }


            //todo Show top three of each country
            var topThreeCarsByCountry =
                manu.GroupJoin(cars, m => m.Name, c => c.Manufacturer,
                (m, g) =>
                    new
                    {
                        Manufacturer = m,
                        Cars = g
                    })
                .GroupBy(m => m.Manufacturer.Headquarter);

            // foreach (var country in topThreeCarsByCountry)
            // {
            //     Console.WriteLine($"{country.Key}");
            //     foreach (var car in country.SelectMany(g => g.Cars).OrderByDescending(c => c.Combined).Take(3))
            //     {
            //         Console.WriteLine($"\t\t{car.Manufacturer, -24} | {car.Name, -24} | {car.Combined}");
            //     }
            // }

            //todo Aggregation 

            // 
            var aggregateMostLeastAverageCarCombinedSyntaxQuery =
                from car in cars
                group car by car.Manufacturer into carGroup
                select new
                {
                    Name = carGroup.Key,
                    Max = carGroup.Max(c => c.Combined),
                    Min = carGroup.Min(c => c.Combined),
                    Ave = carGroup.Average(c => c.Combined)
                } into result
                orderby result.Max descending
                select result;

            // foreach (var aData in aggregateMostLeastAverageCarCombinedSyntaxQuery)
            // {
            //     Console.WriteLine($"{aData.Name}");
            //     Console.WriteLine($"\tMax: {aData.Max}");
            //     Console.WriteLine($"\tMin: {aData.Min}");
            //     Console.WriteLine($"\tAve: {aData.Average}");
            // }


            // Code above runs at least three times per database, we can lower it using Aggregate and class extension (below:)
            var efficientAggregationWithExtend =
                cars.GroupBy(c => c.Manufacturer)
                    .Select(g =>
                    {
                        var results = g.Aggregate(new CarStatistics(),
                                                  (acc, c) => acc.Accumulate(c),
                                                  acc => acc.Compute());
                        return new
                        {
                            Name = g.Key,
                            Ave = results.Average,
                            Min = results.Min,
                            Max = results.Max
                        };
                    })
                    .OrderByDescending(r => r.Max);

            foreach (var aData in efficientAggregationWithExtend)
            {
                Console.WriteLine($"{aData.Name}");
                Console.WriteLine($"\tMax: {aData.Max}");
                Console.WriteLine($"\tMin: {aData.Min}");
                Console.WriteLine($"\tAve: {aData.Ave}");
            }
        }

        private static List<Car> ProcessCarsFile(string fileName)
        {
            var CarsList = new List<Car> { };
            if (CheckFileExists(fileName))
            {
                return File.ReadAllLines($"{FILE_PATH}{fileName}")
                    .Skip(1)
                    .Where(l => l.Length > 1)
                    .ToCar()
                    .ToList();
            };
            return CarsList;
        }

        private static List<Manufacturer> ProcessManufacturerFile(string fileName)
        {
            var ManufacturerList = new List<Manufacturer> { };
            if (CheckFileExists(fileName))
            {
                return File.ReadAllLines($"{FILE_PATH}{fileName}")
                            .Where(l => l.Length > 1)
                            .Select(l =>
                            {
                                var cols = l.Split(',');
                                return new Manufacturer
                                {
                                    Name = cols[0],
                                    Headquarter = cols[1],
                                    Year = int.Parse(cols[2]),
                                };
                            })
                            .ToList();
            }
            return ManufacturerList;
        }

        private static bool CheckFileExists(string fileName)
        {
            if (File.Exists($"{FILE_PATH}{fileName}"))
            {
                return true;
            }
            else
            {
                var ex = new FileLoadException();
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        private static string FILE_PATH = "./assets/";
    }

}


// var resultForProjection = cars.Select(c => c.Name);

// foreach (var name in resultForProjection)
// {
//     foreach (var character in name)
//     {
//        // Now working on chars which are a sequenco of string name of cars
//     }
// }

// // The same is achieved using SelectMany
// var resultForProjectionSelectMany = cars.SelectMany(c => c.Name);
// foreach (var character in resultForProjectionSelectMany) {
//     // It is the same as the one above
// }