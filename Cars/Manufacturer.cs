namespace Cars
{
    public class Manufacturer
    {
        public string Name { get; set; }
        public string Headquarter { get; set; }
        public int Year { get; set; }
    }
}